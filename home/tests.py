from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
# Create your tests here.

from .views import index, search

class TestSearch(TestCase):
    def test_search_url_page_is_exist(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_search_func(self):
        found = resolve("/search/")
        self.assertEqual(found.func, search)

    def test_search_using_template(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'home/search.html')

    def test_search_is_exist(self):
        response = Client().get('/search/')
        self.assertContains(response, 'container-search')

class TestSearchEvent(TestCase):
    def test_event_searchbook_url_is_exist(self):
        response = Client().get('/data/?q=book')
        self.assertEqual(response.status_code, 200)