# Generated by Django 3.1.1 on 2020-10-16 01:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20201015_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lesson',
            name='dosen_pengajar',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='lesson',
            name='nama_mata_kuliah',
            field=models.CharField(max_length=50),
        ),
    ]
