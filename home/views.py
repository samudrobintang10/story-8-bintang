from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponse
import requests
import json

nama_pribadi = "Bintang Samudro"

# Create your views here.
def index(request):
    response = {'name' : nama_pribadi}
    return render(request, 'home/index.html', response)

def search(request):
    return render(request, 'home/search.html')

def searchbook(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    retrive = requests.get(url)
    data = json.loads(retrive.content)
    return JsonResponse(data, safe=False)